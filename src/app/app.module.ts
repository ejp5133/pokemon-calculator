import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatExpansionModule} from '@angular/material/expansion';

import { AppComponent } from './app.component';
import { SinglePokemonComponent } from './components/single-pokemon/single-pokemon.component';

import { PokemonService} from './services';
import { PokemonTeamComponent } from './components/pokemon-team/pokemon-team.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { PokemonResolve } from './resolvers/pokemon.resolve';
import { TypeResolve } from './resolvers/type.resolve';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { MoveCalculatorComponent } from './components/move-calculator/move-calculator.component';

const appRoutes: Routes = [
	{path:'pokedex', component: PokedexComponent, resolve: {pokemon: PokemonResolve}},
	{path:'moveCalculator', component: MoveCalculatorComponent, resolve: {types: TypeResolve}}
]

@NgModule({
  declarations: [
    AppComponent,
    SinglePokemonComponent,
	PokemonTeamComponent,
	PokedexComponent,
	MoveCalculatorComponent
  ],
  imports: [
	BrowserModule,
	MatGridListModule,
	MatExpansionModule,
	MatListModule,
	MatTableModule,
	MatCardModule,
	MatButtonModule,
	BrowserAnimationsModule,
	FormsModule,
	ReactiveFormsModule,
	HttpClientModule,
	MatSelectModule,
	MatInputModule,
	MatAutocompleteModule,
	RouterModule.forRoot(
		appRoutes
	)
  ],
  providers: [
	PokemonService,
	PokemonResolve,
	TypeResolve
],
  bootstrap: [AppComponent]
})
export class AppModule { }
