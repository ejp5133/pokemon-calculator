import { Component, OnInit } from '@angular/core';

import { PokemonService } from '../../services';

import { GameVersionModel } from '../../models';

@Component({
  selector: 'app-pokemon-team',
  templateUrl: './pokemon-team.component.html',
  styleUrls: ['./pokemon-team.component.sass']
})
export class PokemonTeamComponent implements OnInit {

	constructor( private pokemonService: PokemonService) { }

	public version: GameVersionModel;

	ngOnInit() {
		this.pokemonService.getVersion().subscribe((result : GameVersionModel)=>{
			this.version = result;
		});
	}

}
