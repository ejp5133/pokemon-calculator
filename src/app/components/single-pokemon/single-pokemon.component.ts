import { Component, OnInit, Input } from '@angular/core';

import { PokemonService } from '../../services';

@Component({
  selector: 'app-single-pokemon',
  templateUrl: './single-pokemon.component.html',
  styleUrls: ['./single-pokemon.component.sass']
})
export class SinglePokemonComponent implements OnInit {

  constructor( private pokemonService: PokemonService) { }

  @Input()
  public version;

  public types;

  ngOnInit() {
	//   this.pokemonService.getTypes().subscribe((res : any) => {
	// 	  this.types = res;
	//   });

	  this.types = this.pokemonService.getTypes();
  }

}
