import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/services';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { DamageModel, PokemonModel } from '../../models'

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.sass']
})
export class PokedexComponent implements OnInit {
	pokemon;
	filteredOptions: Observable<PokemonModel[]>;
	myControl = new FormControl();
	pokemonSelected;
	damageModel = new DamageModel();
	selectedPokemon;
	pokemonTypes: any[] = [];

	constructor(
		private route: ActivatedRoute,
		private pokemonService: PokemonService
	) { }

	ngOnInit() {
		this.route.data.subscribe((data) => {
			this.pokemon = data.pokemon.results;
		});
		
		this.filteredOptions = this.myControl.valueChanges
		.pipe(
			startWith(''),
			map(value => this._filter(value))
		);
	}

	private _filter(value: any): any {

		let filterValue;
		if(typeof value === 'string'){
			filterValue = value.toLowerCase();
		} else {
			filterValue = value.name.toLowerCase();
		}
	
		return this.pokemon.filter(pokemon => pokemon.name.toLowerCase().includes(filterValue));
	  }

	public getSelection(pokemon){
		
		this.damageModel = new DamageModel();

		document.getElementById('pokemonSelector').blur();
		
		this.pokemonSelected = pokemon.url;
		this.pokemonTypes = [];

		this.pokemonService.getPokemonPokedexInfo(pokemon.url).subscribe((data : any) => {
			console.log(data);
			let otherType;
			data.types.forEach(typeRelation => {
				this.pokemonTypes.push(typeRelation.name);
				typeRelation.damage_relations.double_damage_from.forEach(doubleDamage => {
					console.info("silter");
					console.info(this.damageModel);
					// console.info(this.damageModel.doubleDamage.filter(damage => damage.to === doubleDamage.name));
					console.info(this.damageModel);
					if (this.damageModel.doubleDamage.find(damage => damage.to === doubleDamage.name) != undefined) {
						this.damageModel.doubleDamage.splice(this.damageModel.doubleDamage.findIndex(damage => damage.to === doubleDamage.name),1);
						this.damageModel.quadDamage.push({to:doubleDamage.name, from: undefined});
					} else if (this.damageModel.halfDamage.find(damage => damage.to === doubleDamage.name) != undefined){
						this.damageModel.halfDamage.splice(this.damageModel.halfDamage.findIndex(damage => damage.to === doubleDamage.name),1);
					} else if (!this.damageModel.noDamage.find(damage => damage.to === doubleDamage.name) != undefined){
						this.damageModel.doubleDamage.push({to:doubleDamage.name, from: typeRelation.name});
					}
				})
				typeRelation.damage_relations.half_damage_from.forEach(halfDamage => {
					if (this.damageModel.doubleDamage.find(damage => damage.to === halfDamage.name) != undefined) {
						this.damageModel.doubleDamage.splice(this.damageModel.doubleDamage.findIndex(damage => damage.to === halfDamage.name),1);
					} else if (this.damageModel.halfDamage.find(damage => damage.to === halfDamage.name) != undefined) {
						this.damageModel.halfDamage.splice(this.damageModel.halfDamage.findIndex(damage => damage.to === halfDamage.name),1);
						this.damageModel.quarterDamage.push({to:halfDamage.name, from: undefined});
					} else if (!this.damageModel.noDamage.find(damage => damage.to === halfDamage.name) != undefined){
						this.damageModel.halfDamage.push({to:halfDamage.name, from: typeRelation.name});
					}
				})
				typeRelation.damage_relations.no_damage_from.forEach(noDamage => {
					if (this.damageModel.doubleDamage.find(damage => damage.to === noDamage.name) != undefined) {
						this.damageModel.doubleDamage.splice(this.damageModel.doubleDamage.findIndex(damage => damage.to === noDamage.name),1);
					} else if (this.damageModel.halfDamage.find(damage => damage.to === noDamage.name) != undefined) {
						this.damageModel.halfDamage.splice(this.damageModel.halfDamage.findIndex(damage => damage.to === noDamage.name),1);
					} else {
						this.damageModel.noDamage.push({to:noDamage.name, from: typeRelation.name});
					}
				})
				otherType = typeRelation.name;
			})

			this.selectedPokemon = data.pokemon;
			(<HTMLInputElement>document.getElementById('pokemonSelector')).value='';
			console.log(this.damageModel);
		})
	}

	displayFn(pokemonResponce): string | undefined {
		return pokemonResponce ? pokemonResponce.name : undefined;
	}

	clearInput(){
		this.myControl.setValue('');
	}
}
