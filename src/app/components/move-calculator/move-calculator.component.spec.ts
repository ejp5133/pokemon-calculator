import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MoveCalculatorComponent } from './move-calculator.component';

describe('MoveCalculatorComponent', () => {
  let component: MoveCalculatorComponent;
  let fixture: ComponentFixture<MoveCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MoveCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
