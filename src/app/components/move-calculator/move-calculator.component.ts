import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PokemonService } from 'src/app/services';
import { startWith, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';




@Component({
  selector: 'app-move-calculator',
  templateUrl: './move-calculator.component.html',
  styleUrls: ['./move-calculator.component.sass']
})
export class MoveCalculatorComponent implements OnInit {


	//loaded from api call
	types;
	//used to filter the boxes
	filteredOptions: Observable<any[]>;
	myControl = new FormControl();
	typeMappings = [];
	displayedColumns: string[] = ['name', 'number'];
	pokemonArray: any[] = [];
	typeSpaces = [];
	activeSlide = 0; // NEW: the current slide # (0 = first)
	slideCount = 3;

	//second page shit
	typeSuggestion =[];
	highestMapping = 0;
	lowestMapping = 1000;

  	constructor(
		private route: ActivatedRoute,
		private pokemonService: PokemonService
	) { }

	ngOnInit() {
		this.route.data.subscribe((data) => {
			this.types = data.types;

			//old
			for(var i = 1; i <= 24; i++){
				this.typeSpaces.push({option: i, type: undefined});
			}
			 
			//new
			for(var i = 1; i <= 6; i++){
				// let pokemonMoves: any[] = [];
				// for(var i = 1; i <= 4; i++){
				// 	pokemonMoves.push({option: i, type: undefined});
				// }
				this.pokemonArray.push({space: i, pokemon: undefined, type: [
					{option: 1, type: undefined},
					{option: 2, type: undefined},
					{option: 3, type: undefined},
					{option: 4, type: undefined}
				]});
			}

			this.types.forEach((type) => {
				this.typeMappings.push({name: type.name, number: 0});
			})

			this.suggestImportantType()
		});



		this.filteredOptions = this.myControl.valueChanges
		.pipe(
			startWith(''),
			map(value => this._filter(value))
		);
	}

	private _filter(value: any): any {

		let filterValue;

		if(typeof value === 'string'){
			filterValue = value.toLowerCase();
		} else {
			filterValue = value.name.toLowerCase();
		}

		return this.types.filter(type => type.name.toLowerCase().includes(filterValue));
	}

	displayTypeForSelect(pokemonResponce): string | undefined {
		return pokemonResponce ? pokemonResponce.name : undefined;
	}

	public getSelection(pokemonNumber, typeNumber, type){
		document.getElementById('typeSelector'+pokemonNumber+typeNumber).blur();

		//this.filteredOptions = this.types;
		this.filteredOptions = this.myControl.valueChanges
		.pipe(
			startWith(''),
			map(value => this._filter(value))
		);

		this.pokemonArray.forEach(pokemon => {
			if(pokemon.space === pokemonNumber){
				pokemon.type.forEach(space => {
					if(typeNumber === space.option){
						if (space.type !== undefined){
							//remove old
							space.type.damage_relations.double_damage_to.forEach((doubleDamage => {
								this.typeMappings.forEach(typeMapping => {
									if(typeMapping.name === doubleDamage.name){
										typeMapping.number = typeMapping.number - 1;
									}
								})
							}))
						}
						//overwrite old type selection
						space.type = type;
						//redo calculation
						type.damage_relations.double_damage_to.forEach((doubleDamage => {
							this.typeMappings.forEach(typeMapping => {
								if(typeMapping.name === doubleDamage.name){
									typeMapping.number = typeMapping.number + 1;
								}
							})
						}))
					}
				})
			}
		})
		this.suggestImportantType()
	}

	public suggestImportantType(){
		this.typeSuggestion = [];
		this.updateHighestLowestMapping()
		
		this.types.forEach((type: any) => {
			let totalPoints=0;
			type.damage_relations.double_damage_to.forEach(damagedType => {
			this.typeMappings.forEach((typeMapping: any) => {
					if(damagedType.name === typeMapping.name){
						// this.typeSuggestion.splice(this.typeSuggestion.indexOf(this.typeSuggestion.filter(type => type.name.toLowerCase().includes(typeMapping.name))));

						totalPoints = totalPoints + this.calculateTypeSuggestionRank(typeMapping.number);
					}
				})
			})
			this.typeSuggestion.push({name: type.name, number: totalPoints });
		})
		this.typeSuggestion.sort((a,b) => {
			if(a.number > b.number){
				return -1;
			} else if (a.number < b.number){
				return 1;
			} 
			return 0;
		});
		console.info(this.typeSuggestion);
	}

	public calculateTypeSuggestionRank(currentStanding){
		let multiplier = 1;

		if (currentStanding === this.lowestMapping){
			multiplier = 5
		}

		return (this.highestMapping + 1 - currentStanding) * multiplier;
	}

	public updateHighestLowestMapping(){
		this.lowestMapping = 1000;
		this.typeMappings.forEach( type => {
			if(type.name !== 'unknown' && type.name !== 'shadow'){
				if (type.number > this.highestMapping) {
					this.highestMapping = type.number
				}
				if (type.number < this.lowestMapping) {
					this.lowestMapping = type.number
				}
			}
		});
		console.info('highest:' + this.highestMapping);
		console.info('highest:' + this.lowestMapping);
	}

	onPan(e){
		var sliderEl = <HTMLElement>document.getElementsByClassName('slider')[0];
		var percentage = 100 / this.slideCount * e.deltaX / window.innerWidth; // NEW: our % calc
		var transformPercentage = percentage - 100 / this.slideCount * this.activeSlide; // NEW
		sliderEl.style.transform = 'translateX( ' + transformPercentage + '% )';
		if(e.isFinal) { // NEW: this only runs on event end
			if(percentage < 0)
				this.goToSlide(this.activeSlide + 1);
			else if(percentage > 0)
				this.goToSlide(this.activeSlide - 1);
			else
				this.goToSlide(this.activeSlide);
		}
	}

	public goToSlide(number){
		var sliderEl = <HTMLElement>document.getElementsByClassName('slider')[0];
		if(number < 0)
		this.activeSlide = 0;
		else if(number > this.slideCount - 1)
		this.activeSlide = this.slideCount - 1
		else
		this.activeSlide = number;

		sliderEl.classList.add( 'is-animating' );

		var percentage = -(100 / this.slideCount) * this.activeSlide;
		sliderEl.style.transform = 'translateX(' + percentage + '%)';
		clearTimeout( timer );
		var timer = setTimeout( function() {
			sliderEl.classList.remove( 'is-animating' );
		}, 400 );
	}
}
