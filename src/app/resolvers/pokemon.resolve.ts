import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PokemonService } from '../services';
import { map } from 'rxjs/operators';

@Injectable()
export class PokemonResolve implements Resolve<any> {

	constructor(private pokemonService: PokemonService){}

	public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
		return this.pokemonService.getAllPokemon().pipe(
			map((data) => {
				return data
			})
		);
	}
}