import { Injectable } from "@angular/core";
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PokemonService } from '../services';
import { map } from 'rxjs/operators';

@Injectable()
export class TypeResolve implements Resolve<any> {

	constructor(private pokemonService: PokemonService){}

	public resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
		return this.pokemonService.getAllTypes().pipe(
			map((data) => {
				return data;
			})
		);
	}
}