export class DamageModel {
	quadDamage: any[] = [];
	doubleDamage: any[] = [];
	halfDamage: any[] = [];
	quarterDamage: any[] = [];
	noDamage: any[] = [];

}