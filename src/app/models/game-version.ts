export class GameVersionModel {
	count: number;
	results: [{
		name: string;
		url: string;
	}]
}