export class PokemonModel {
	count: number;
	results: [{
		name: string;
		url: string;
	}]
}