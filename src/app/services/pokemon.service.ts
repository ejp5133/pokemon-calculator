import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { GameVersionModel } from './../models';
import { throwError, forkJoin, Observable } from 'rxjs';

import { catchError, map, mergeMap, tap } from 'rxjs/operators';

@Injectable()
export class PokemonService {

	private pokemonApiEndpoint = 'https://pokeapi.co/api/v2/'
  	constructor(private http: HttpClient) { }
	observableArray = [];
    getTypes(){
        return this.http.get(`${this.pokemonApiEndpoint}type`).pipe(
			catchError(this.handleError)
		);
	}
	
	getVersion(){
		return this.http.get(`${this.pokemonApiEndpoint}version-group`)
		.pipe(
			catchError(this.handleError)
		);
	}

	getPokemonByVersion(version: string){
		return this.http.get(`${this.pokemonApiEndpoint}version-group/${version}`)
		.pipe(
			catchError(this.handleError)
		);
	}

	getAllPokemon(){
		return this.http.get(`${this.pokemonApiEndpoint}pokemon?limit=1000`)
		.pipe(
			map((data) => {
				return data
			}),
			catchError(this.handleError)
			
		);
	}

	getGenericPokemonCall(url){
		return this.http.get(url)
		.pipe(
			map((data) => {
				return data
			}),
			catchError(this.handleError)
			
		);
	}

	getPokemonPokedexInfoc(url){
		return this.http.get(url)
		.pipe(
			tap((data: any) => {
				data.types.forEach((type : any ) => {
					this.observableArray.push(this.http.get(type.url));
				})
			}),
				
		);
	}

	//works
	getPokemonPokedexInfo(url){
		return this.http.get(url)
		.pipe(
			mergeMap((data : any) => {
				let thisthing = [];
				data.types.forEach((type : any ) => {
					thisthing.push(this.http.get(type.type.url));
				})
				// return new Observable((observer) => {
				// 	let typesObject;
				// 	return forkJoin(thisthing).subscribe((types) => {
				// 		return {types: types, pokemon: data};// {}typesObject = data})
				// 	// let thsaf:string[] =  [{types: typesObject, pokemon: data}]
				// 	observer.next(1);
				// 	// observer.complete();
				// });	
			// })
				return Observable.create((observer) => {
					let typesObject;
					forkJoin(thisthing).subscribe((types) => {
						typesObject = types;
						observer.next({types: typesObject, pokemon: data});
						observer.complete();
					})
						// return {types: types, pokemon: data};// {}typesObject = data})
						// let thsaf:string[] =  [{types: typesObject, pokemon: data}]
					
					// observer.complete();
				});

				
			})
				
			// })
		)
	}

	//works
	getPokemonPokedexInfoa(url){
		return this.http.get(url)
		.pipe(
			mergeMap((data : any) => {
					return this.http.get(data.types[0].type.url)
			}),
				
		);
	}

	getAllTypes(){
		return this.http.get(`${this.pokemonApiEndpoint}type`)
			.pipe(
				mergeMap((data : any) => {
					let requestsArray = [];
					data.results.forEach((type : any ) => {
						requestsArray.push(this.http.get(type.url));
					});
					
					return forkJoin(requestsArray);
				})
			)
	}

	private handleError(error: HttpErrorResponse) {
		if (error.error instanceof ErrorEvent) {
		  // A client-side or network error occurred. Handle it accordingly.
		  console.error('An error occurred:', error.error.message);
		} else {
		  // The backend returned an unsuccessful response code.
		  // The response body may contain clues as to what went wrong,
		  console.error(
			`Backend returned code ${error.status}, ` +
			`body was: ${error.error}`);
		}
		// return an observable with a user-facing error message
		return throwError(
		  'Something bad happened; please try again later.');
	  };
}